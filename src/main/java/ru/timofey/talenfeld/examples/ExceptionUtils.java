package ru.timofey.talenfeld.examples;

/**
 * Created by root on 3/14/16.
 */
public class ExceptionUtils {

    /**
     * this method provides informations about method that called it
     */

    public static String getCallerClassAndMethodName() {
        if(Thread.currentThread().getStackTrace().length < 4) {
            return null;
        }
        return Thread.currentThread().getStackTrace()[3].getClassName() + "#" + Thread.currentThread().getStackTrace()[3].getMethodName();

    }

}
