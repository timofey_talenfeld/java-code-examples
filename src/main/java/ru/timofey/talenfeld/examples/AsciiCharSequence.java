package ru.timofey.talenfeld.examples;

/**
 *
 * This class allows to storage ascii sequence. Insead of 2 bytes, every symbol takes only one byte.
 *
 * Created by root on 3/14/16.
 */
public class AsciiCharSequence implements CharSequence {

    private byte[] array;

    public AsciiCharSequence(byte[] content) {
        this.array = content;
    }

    @Override
    public int length() {
        return array.length;
    }

    @Override
    public char charAt(int index) {
        if(index > array.length-1) {
            return (char)array[array.length-1];
        }
        if(index < 0) {
            return (char)array[0];
        }
        return (char)array[index];
    }

    @Override
    public CharSequence subSequence(int start, int end) {
        byte[] tmp = new byte[end-start];

        for(int i = start; i < end; ++i) {
            tmp[i-start] = array[i];
        }

        return new AsciiCharSequence(tmp);
    }

    @Override
    public String toString() {
        return new String(array);
    }
}
