package ru.timofey.talenfeld.examples;

/**
 * it's cascade of classes that works with interface MailService
 * I've put all of my code here, because it's very difficult to make packages and files for each class.
 * It's only example
 *
 * Created by root on 3/14/16.
 */
public static class UntrustworthyMailWorker implements MailService {

    private MailService[] mailServices;
    private RealMailService realMailService = new RealMailService();

    public UntrustworthyMailWorker(MailService[] mailServices) {
        this.mailServices = mailServices;
    }

    public Sendable processMail(Sendable mail) {
        Sendable current = mail;
        Sendable next = null;

        for(int i = 0; i < mailServices.length; ++i) {
            next = mailServices[i].processMail(current);
            current = next;
        }
        return realMailService.processMail(current);
    }

    public RealMailService getRealMailService() {
        return realMailService;
    }
}

public static class Spy implements MailService {

    private final Logger logger;

    public Spy(Logger logger) {
        this.logger = logger;
    }

    public Sendable processMail(Sendable mail) {
        if(mail instanceof MailMessage) {
            if(mail.getFrom().equals(AUSTIN_POWERS) || mail.getTo().equals(AUSTIN_POWERS)) {
                logger.log(Level.WARNING, "Detected target mail correspondence: from {0} to {1} \"{2}\"", new Object[] {mail.getFrom(), mail.getTo(), ((MailMessage)mail).getMessage()});
            } else {
                logger.log(Level.INFO, "Usual correspondence: from {0} to {1}",new Object [] {mail.getFrom(),mail.getTo()});
            }
        }
        return mail;
    }

}

public static class Thief implements MailService {

    private int price, sum;

    public Thief(int price) {
        this.price = price;
    }

    public Sendable processMail(Sendable mail) {
        if(mail instanceof MailPackage) {
            if(((MailPackage)mail).getContent().getPrice() >= price) {
                sum += ((MailPackage)mail).getContent().getPrice();

                return new MailPackage(mail.getFrom(),mail.getTo(),new Package("stones instead of " + ((MailPackage)mail).getContent().getContent(),0));
            }
        }
        return mail;
    }

    public int getStolenValue() {
        return sum;
    }
}

public static class Inspector implements MailService {
    public Inspector() {}

    public Sendable processMail(Sendable mail) {

        if(mail instanceof MailPackage) {
            String content = ((MailPackage)mail).getContent().getContent();

            if(content.contains(WEAPONS) || content.contains(BANNED_SUBSTANCE)) {
                throw new IllegalPackageException();
            }

            if(content.contains("stones")) {
                throw new StolenPackageException();
            }
        }

        return mail;
    }

}

public static class IllegalPackageException extends RuntimeException {
    public IllegalPackageException() {}
}

public static class StolenPackageException extends RuntimeException {
    public StolenPackageException() {}
}

