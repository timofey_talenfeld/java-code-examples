package ru.timofey.talenfeld.examples;

/**
 *
 * analog of std::pair in C++
 *
 * Created by root on 3/14/16.
 */
class Pair <F,S> {

    private F first;
    private S second;

    private Pair(F first, S second) {
        this.first = first;
        this.second = second;
    }

    public F getFirst() {
        return first;
    }

    public S getSecond() {
        return second;
    }

    public static <F,S> Pair<F,S> of(F first, S second) {
        return new Pair<F,S>(first,second);
    }

    public boolean equals(Object other) {
        if(other == null) {
            return false;
        }
        if(!(other instanceof Pair)) {
            return false;
        }
        if((this.first == null) || (this.second == null) || (((Pair<?,?>)other).first == null) || (((Pair<?,?>)other).second == null)) {
            return true;
        }
        if(first.equals(((Pair<?,?>)other).first) && second.equals(((Pair<?,?>)other).second)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        if((first == null) || second == null) {
            return 0;
        }
        return first.hashCode() + second.hashCode();
    }

}
