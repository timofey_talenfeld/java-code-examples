package ru.timofey.talenfeld.examples;

import java.io.ByteArrayInputStream;
import java.io.ObjectInputStream;

/**
 * Created by root on 3/14/16.
 */
public class SerializableUtils {

    /**
     * this method deserialize array of bytes to array of Animal.
     * array of bytes contains the length of array of Anumal (first 4 bytes)
     *
     */

    public static Animal[] deserializeAnimalArray(byte[] data) {
        if(data.length < 4) {
            throw new IllegalArgumentException();
        }

        ByteArrayInputStream b = null;
        ObjectInputStream o = null;

        try {
            b = new ByteArrayInputStream(data);
            o = new ObjectInputStream(b);
        }catch(Exception e) {
            throw new IllegalArgumentException();
        }

        int length = 0;

        try {
            length = o.readInt();
        }catch(Exception e) {
            throw new IllegalArgumentException();}

        if(length < 0) {
            throw new IllegalArgumentException();
        }

        Animal[] animals = new Animal[length];

        for(int i = 0; i < length; ++i) {
            try {
                animals[i] = (Animal)o.readObject();
            }catch(Exception e) {
                throw new IllegalArgumentException();
            }
        }

        return animals;
    }

}
