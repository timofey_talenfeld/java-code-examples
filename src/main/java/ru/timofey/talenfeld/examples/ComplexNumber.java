package ru.timofey.talenfeld.examples;

/**
 * Created by root on 3/14/16.
 */
public class ComplexNumber {
    private final double re;
    private final double im;

    public ComplexNumber(double re, double im) {
        this.re = re;
        this.im = im;
    }

    public boolean equals(ComplexNumber other) {
        if(!(other instanceof ComplexNumber)) {
            return false;
        }
        if(this == other) {
            return true;
        }
        if(this.re == other.getRe() && this.im == other.getIm()) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        return (int)(re+im);
    }

    public double getRe() {
        return re;
    }

    public double getIm() {
        return im;
    }
}
